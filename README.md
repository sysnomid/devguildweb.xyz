<div align="center">

![](https://cdn.discordapp.com/attachments/718205418723213382/733202847331647508/TDG.png)

# The Developer's Guild Discord Site Files

### Site Files for https://devguildweb.xyz
https://discord.gg/xqUN8KY

--------------------------------------------------------------------

**Copyright Sysnomid, 2020 | https://sysnomid.com**, under Apache 2.0 License

**DGW Archives, from https://github.com/Tyrrrz/DiscordChatExporter/wiki, under GPLv2**

----------------------------------------------------------------------
</div>

**To Install:**

1. Get node 12 from nvm

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
nvm install 12
nvm use 12

```

2. Run ``` npm install ```

3. run ```node ./bin/www```
or ```pm2 start bin/www``` or for cluster mode(https://pm2.keymetrics.io/docs/usage/cluster-mode/) 
```pm2 start bin/www -i max```

----------------------------------------------------------------------
**(Optional) To Access Archived Threads of Developer's Guild Discord:**
1. Install .NET Core 3.5,  https://dotnet.microsoft.com/download
2. Follow The Wiki Page of DCE, https://github.com/Tyrrrz/DiscordChatExporter/wiki
